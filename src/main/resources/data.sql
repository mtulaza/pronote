-- users
INSERT INTO "users"("id","username", "password", "enabled") VALUES (1, 'david', '$2a$10$QuWLqGw4eggvdk0KArkAvO78VdPEq1QMaYY8c8cuAiEtogqcx5v/e', 1);
INSERT INTO "users"("id","username", "password", "enabled") VALUES (2, 'mark', '$2a$10$TqkVKtLK0p5SD4OD2AsKQebfKcmLBc8yC5BvQoFk1mvxvl26wUuiS', 1);
INSERT INTO "users"("id","username", "password", "enabled") VALUES (3, 'john', '$2a$10$mkwS7UoxFTDg6PLKGr428O94iIgo9IZjFCT0hA/50.V19uyc9RipK', 1);

-- role
INSERT INTO "role"("id", "rolename", "user_id") VALUES (1, 'ROLE_ADMIN', 1);
INSERT INTO "role"("id", "rolename", "user_id") VALUES (2, 'ROLE_USER', 1);
INSERT INTO "role"("id", "rolename", "user_id") VALUES (3, 'ROLE_USER', 2);
INSERT INTO "role"("id", "rolename", "user_id") VALUES (4, 'ROLE_USER', 3);

