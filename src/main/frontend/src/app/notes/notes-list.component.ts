import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";

@Component({
  selector: 'notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.css']
})
export class NotesListComponent implements OnInit {
  public notes: Array<Note>;
  public isLoading: boolean;
  public groupId: number;

  constructor(private activatedRoute: ActivatedRoute,
              private http: Http) {
  }

  ngOnInit() {
    this.groupId = this.activatedRoute.snapshot.params['groupId'];

    this.pullNotes();
  }

  private pullNotes() {
    this.http.get('/note?groupId=' + this.groupId)
      .map(resp => resp.json())
      .map(notes => notes as Array<Note>)
      .finally(() =>
        setTimeout(() => {
          this.isLoading = false;
        }, 500)
      )
      .subscribe(notes => this.notes = notes);
  }

  addNote(title: string, description: string) {
    const payload = {
      title: title,
      description: description,
      groupId: this.groupId
    };

    this.http.post('/note', payload)
      .subscribe(() => {
        this.pullNotes();
      })
  }

  removeNote(noteId: number) {
    this.http.delete('/note/' + noteId)
      .subscribe(() => {
        this.pullNotes();
      })
  }

  toggleCompleted(noteId: number) {
    this.http.put('/note/' + noteId, null)
      .subscribe(() => {
        this.pullNotes();
      })
  }
}
