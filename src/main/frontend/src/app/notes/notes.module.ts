import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { GroupListComponent } from "./group-list.component";
import { SharedModule } from "../shared/shared.module";
import { NotesRoutingModule } from "./notes-routing.module";
import { NotesListComponent } from "./notes-list.component";



@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgbModule,
    SharedModule,
    NotesRoutingModule
  ],
  declarations: [
    GroupListComponent,
    NotesListComponent
  ],
  providers: []
})
export class NotesModule {
}
