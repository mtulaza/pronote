import { Component, OnInit } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";

@Component({
  selector: 'group-list',
  templateUrl: './group-list.component.html',
  styleUrls: [
    './group-list.component.css'
  ]
})
export class GroupListComponent implements OnInit {
  public isLoading: boolean;
  public groups: Array<Group>;

  constructor(private http: Http) {
  }

  ngOnInit() {
    this.isLoading = true;
    this.pullGroups();
  }

  pullGroups() {
    this.http.get('/group')
      .map(resp => resp.json())
      .map(groups => groups.map((group: any) => <Group> {
        id: group.id,
        name: group.name,
        notesNo: group.notes.length
      }))
      .finally(() =>
        setTimeout(() => {
          this.isLoading = false;
        }, 500)
      )
      .subscribe(groups => this.groups = groups);
  }

  addGroup(groupName: string) {
    if (groupName == '' || groupName == null) {
      return;
    }

    const payload = {
      groupName: groupName
    };

    this.http.post('/group', payload)
      .subscribe(() => {
        this.pullGroups();
      });
  }

  removeGroup(id: number) {
    this.http.delete('/group/' + id)
      .subscribe(() => {
        this.pullGroups();
      });
  }
}
