import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { GroupListComponent } from "./group-list.component";
import { Route } from "../core/route.service";
import { NotesListComponent } from "./notes-list.component";

const routes: Routes = Route.withShell([
  { path: 'groups', component: GroupListComponent },
  { path: 'notes/:groupId', component: NotesListComponent },
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class NotesRoutingModule {
}
