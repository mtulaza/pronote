import { NgModule, Optional, SkipSelf } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { Http, HttpModule, RequestOptions, XHRBackend } from "@angular/http";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { ShellComponent } from "./shell/shell.component";
import { HeaderComponent } from "./shell/header/header.component";
import { AuthenticationService } from "./authentication/authentication.service";
import { AuthenticationGuard } from "./authentication/authentication.guard";
import { HttpService } from "./http/http.service";
import { ApiXHRBackend } from "./http/api-xhr-backend";

import { CarouselComponent } from './../carousel/carousel.component';
import { CarouselItemComponent } from './../carousel-item/carousel-item.component';

export function createHttpService(backend: XHRBackend,
                                  defaultOptions: RequestOptions) {
  return new HttpService(backend, defaultOptions);
}

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    NgbModule,
    RouterModule
  ],
  declarations: [
    HeaderComponent,
    ShellComponent,
CarouselComponent,
CarouselItemComponent
  ],
  providers: [
    AuthenticationService,
    AuthenticationGuard,
    {
      provide: Http,
      deps: [XHRBackend, RequestOptions],
      useFactory: createHttpService
    },
    {
      provide: XHRBackend,
      useClass: ApiXHRBackend
    }
  ]
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    // Import guard
    if (parentModule) {
      throw new Error(`${parentModule} has already been loaded. Import Core module in the AppModule only.`);
    }
  }

}
