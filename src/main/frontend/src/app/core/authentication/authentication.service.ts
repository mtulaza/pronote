import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { Http, RequestOptionsArgs, Headers, Response } from "@angular/http";
import { HttpParams } from "@angular/common/http";

export interface Credentials {
  username: string;
  isAdmin: boolean;
}

export interface LoginContext {
  username: string;
  password: string;
  remember?: boolean;
}

const credentialsKey = 'credentials';

@Injectable()
export class AuthenticationService {

  private _credentials: Credentials;

  constructor(private http: Http) {
    this._credentials = JSON.parse(sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey));
  }

  /**
   * Authenticates the user.
   * @param {LoginContext} context The login parameters.
   * @return {Observable<Credentials>} The user credentials.
   */
  login(context: LoginContext): Observable<Credentials> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    const params = new HttpParams()
      .set('username', context.username)
      .set('password', context.password);

    return this.http
      .post('/authentication', params.toString(), <RequestOptionsArgs> {
        headers: headers,
      })
      .map((response: Response) => {
        const body = response.json();
        const credentials = <Credentials> {
          username: body.username,
          isAdmin: body.isAdmin,
        };

        this.setCredentials(credentials);

        return credentials;
      });
  }

  /**
   * Logs out the user and clear credentials.
   * @return {Observable<boolean>} True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    return this.http.post("/logout", null)
      .finally(() => this.setCredentials())
      .map(resp => true);
  }

  /**
   * Checks is the user is authenticated.
   * @return {boolean} True if the user is authenticated.
   */
  isAuthenticated(): boolean {
    return !!this.credentials;
  }

  /**
   * Gets the user credentials.
   * @return {Credentials} The user credentials or null if the user is not authenticated.
   */
  get credentials(): Credentials {
    return this._credentials;
  }

  /**
   * Sets the user credentials.
   * The credentials may be persisted across sessions by setting the `remember` parameter to true.
   * Otherwise, the credentials are only persisted for the current session.
   * @param {Credentials=} credentials The user credentials.
   * @param {boolean=} remember True to remember credentials across sessions.
   */
  private setCredentials(credentials?: Credentials, remember?: boolean) {
    this._credentials = credentials || null;

    if (credentials) {
      const storage = remember ? localStorage : sessionStorage;
      storage.setItem(credentialsKey, JSON.stringify(credentials));
    } else {
      sessionStorage.removeItem(credentialsKey);
      localStorage.removeItem(credentialsKey);
    }
  }

}
