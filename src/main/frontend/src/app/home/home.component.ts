import "rxjs/add/operator/finally";

import { Component, OnInit } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [
    './home.component.css'
  ]
})
export class HomeComponent implements OnInit {
  public lastThreeNotes: Array<Note>;

  constructor(private http: Http) {
  }

  ngOnInit() {
    this.pullLastThreeNotes();
  }

  private pullLastThreeNotes() {
    this.http.get('/note/last-three')
      .map(resp => resp.json())
      .map(lastThreeNotes => lastThreeNotes as Array<Note>)
      .subscribe(lastThreeNotes => this.lastThreeNotes = lastThreeNotes);
  }
}
