interface Note {
  readonly id: number;
  readonly title: string;
  readonly description: string;
  readonly creationDate: string;
  readonly completed: boolean;
}
