
interface Group {
  readonly id: number;
  readonly name: string;
  readonly notesNo: number;
}
