package com.pronote.views;

import com.pronote.domain.Note;
import org.springframework.stereotype.Component;

@Component
public class NoteViewFactory {
    public NoteView map(Note note) {
        NoteView view = new NoteView();
        view.setId(note.getId());
        view.setTitle(note.getTitle());
        view.setDescription(note.getDescription());
        view.setCreationDate(note.getCreationDate());
        view.setCompleted(note.isCompleted());

        return view;
    }
}
