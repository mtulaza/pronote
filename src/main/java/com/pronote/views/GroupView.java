package com.pronote.views;

import java.util.Collection;

public class GroupView {
    private Integer id;
    private String name;
    private Collection<NoteView> notes;
    private UserView owner;

    public GroupView() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<NoteView> getNotes() {
        return notes;
    }

    public void setNotes(Collection<NoteView> notes) {
        this.notes = notes;
    }

    public UserView getOwner() {
        return owner;
    }

    public void setOwner(UserView owner) {
        this.owner = owner;
    }
}
