package com.pronote.views;

import com.pronote.domain.Role;
import com.pronote.domain.RoleName;
import com.pronote.domain.User;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserViewFactory {

    public User map(UserView dto) {
        User user = new User();
        user.setUsername(dto.getUsername());
        user.setEnabled(true);
        user.setRoles(buildDomainRoles(user, dto.getRoles()));
        user.setCreationTime(LocalDateTime.now());

        return user;
    }

    private Set<Role> buildDomainRoles(User user, Set<RoleName> roleNames) {
        return roleNames.stream()
                .map(roleName -> {
                    Role role = new Role();
                    role.setUser(user);
                    role.setRolename(roleName);

                    return role;
                })
                .collect(Collectors.toSet());
    }

    public UserView map(User user) {
        UserView dto = new UserView();
        dto.setId(dto.getId());
        dto.setUsername(user.getUsername());
        dto.setEnabled(true);
        dto.setRoles(user.getRoles().stream().map(Role::getRolename).collect(Collectors.toSet()));
        dto.setCreationTime(user.getCreationTime());
        dto.setModificationTime(user.getModificationTime());

        return dto;
    }
}
