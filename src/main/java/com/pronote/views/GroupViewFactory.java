package com.pronote.views;

import com.pronote.domain.Group;
import com.pronote.domain.Note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GroupViewFactory {
    @Autowired
    private NoteViewFactory noteViewFactory;
    @Autowired
    private UserViewFactory userViewFactory;

    public GroupView map(Group group) {
        GroupView view = new GroupView();
        view.setId(group.getId());
        view.setName(group.getName());
        view.setNotes(buildNoteViews(group.getNotes()));
        view.setOwner(userViewFactory.map(group.getOwner()));

        return view;
    }

    private List<NoteView> buildNoteViews(Collection<Note> notes) {
        return notes.stream()
                .map(noteViewFactory::map)
                .collect(Collectors.toList());
    }
}
