package com.pronote.users;

import java.util.Optional;

import com.pronote.support.jpa.CustomJpaRepository;
import com.pronote.domain.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CustomJpaRepository<User, Integer> {
	Optional<User> findByUsername(final String username);
}
