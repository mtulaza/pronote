package com.pronote.users;

import com.pronote.domain.Role;
import com.pronote.domain.RoleName;
import com.pronote.domain.User;
import com.pronote.views.UserView;
import com.pronote.views.UserViewFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserViewFactory userViewFactory;

    @Transactional
    public void createUser(final UserView userView, final RoleName roleName) {
        final User user = userViewFactory.map(userView);
        final Role role = new Role();
        role.setRolename(roleName);
        role.setUser(user);
        user.getRoles().add(role);
        user.setEnabled(true);

        userRepository.save(user);
    }
    
    public void updateUser(UserView user) {
        throw new RuntimeException("User update is not yet implemented");
    }

    @Transactional(readOnly = true)
    public List<UserView> getUsers() {
        return userRepository.findAll(new Sort("id")).stream()
                .map(user -> userViewFactory.map(user))
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public UserView getUser(final Integer id) {
        return userViewFactory.map(find(id));
    }

    @Transactional
    public void deleteUser(final Integer id) {
        userRepository.delete(id);
    }

    @Transactional(readOnly = true)
    private User find(final Integer id) {
        return userRepository
                .findOne(id)
                .orElseThrow(() -> new UserNotFoundException(id));
    }

}
