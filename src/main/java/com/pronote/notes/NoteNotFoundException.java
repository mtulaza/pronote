package com.pronote.notes;

public class NoteNotFoundException extends RuntimeException {
    public NoteNotFoundException(final int id) {
        super("Cannot find note of id: " + id);
    }
}
