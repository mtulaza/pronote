package com.pronote.notes;

import com.pronote.domain.Note;
import com.pronote.support.jpa.CustomJpaRepository;

import java.util.Collection;

public interface NotesRepository extends CustomJpaRepository<Note, Integer> {
    Collection<Note> findByGroupIdOrderByCreationDateDesc(final Integer groupId);
    Collection<Note> findFirst3ByOrderByCreationDateDesc();
}
