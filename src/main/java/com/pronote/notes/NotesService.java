package com.pronote.notes;

import com.pronote.domain.Group;
import com.pronote.domain.Note;
import com.pronote.groups.GroupNotFoundException;
import com.pronote.groups.GroupsRepository;
import com.pronote.notes.data.CreateNoteData;
import com.pronote.views.NoteView;
import com.pronote.views.NoteViewFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class NotesService {
    @Autowired
    private NotesRepository notesRepository;
    @Autowired
    private GroupsRepository groupsRepository;
    @Autowired
    private NoteViewFactory noteViewFactory;

    @Transactional(readOnly = true)
    public Optional<NoteView> findById(final int id) {
        return notesRepository.findOne(id)
                .map(noteViewFactory::map);
    }

    @Transactional
    public int createNote(final CreateNoteData createNoteData) {
        final Group group = groupsRepository.findOne(createNoteData.getGroupId())
                .orElseThrow(() -> new GroupNotFoundException(createNoteData.getGroupId()));

        Note note = new Note();
        note.setTitle(createNoteData.getTitle());
        note.setDescription(createNoteData.getDescription());
        note.setGroup(group);
        note.setCompleted(false);
        note.setCreationDate(LocalDateTime.now());

        group.getNotes().add(note);

        Note createdNote = notesRepository.save(note);
        groupsRepository.save(group);

        return createdNote.getId();
    }

    @Transactional(readOnly = true)
    public List<NoteView> getRecentNotes(final int notesNo, final int groupId) {
        return notesRepository.findByGroupIdOrderByCreationDateDesc(groupId).stream()
                .limit(notesNo)
                .map(noteViewFactory::map)
                .collect(Collectors.toList());
    }

    @Transactional
    public void toggleCompleted(final int noteId) {
        Note note = notesRepository.findOne(noteId)
                .orElseThrow(() -> new NoteNotFoundException(noteId));

        note.setCompleted(!note.isCompleted());

        notesRepository.save(note);
    }

    @Transactional(readOnly = true)
    public List<NoteView> getAll(final int groupId) {
        Collection<Note> notes = notesRepository.findByGroupIdOrderByCreationDateDesc(groupId);

        return notes.stream()
                .map(noteViewFactory::map)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<NoteView> getLastThreeNotes() {
        return notesRepository.findFirst3ByOrderByCreationDateDesc().stream()
                .map(noteViewFactory::map)
                .collect(Collectors.toList());
    }

    @Transactional
    public void deleteNote(final int id) {
        notesRepository.delete(id);
    }
}
