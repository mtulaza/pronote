package com.pronote.notes;

import com.pronote.notes.data.CreateNoteData;
import com.pronote.views.NoteView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = NotesController.BASE_URI)
public class NotesController {
    public final static String BASE_URI = "/note";
    public final static String RECENT_URI = "/recent";
    private final static ResponseEntity<NoteView> BAD_REQUEST_RESPONSE = new ResponseEntity<>(HttpStatus.BAD_REQUEST);

    @Autowired
    private NotesService notesService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getNoteById(@PathVariable final int id) {
        final Optional<NoteView> note = notesService.findById(id);

        return note
                .map(ResponseEntity::ok)
                .orElse(BAD_REQUEST_RESPONSE);
    }

    @PostMapping
    public ResponseEntity<?> createNote(@RequestBody CreateNoteData createNoteData) {
        final int createdNoteId = notesService.createNote(createNoteData);

        return ResponseEntity
                .created(URI.create(BASE_URI + "/" + createdNoteId))
                .build();
    }

    @GetMapping(value = RECENT_URI + "/{notesNo}")
    public ResponseEntity<?> getRecentNotes(@PathVariable final int notesNo,
                                            @RequestParam final int groupId) {
        final List<NoteView> recentNotes = notesService.getRecentNotes(notesNo, groupId);

        return ResponseEntity.ok(recentNotes);
    }

    @GetMapping
    public ResponseEntity<?> getAllNotes(@RequestParam final int groupId) {
        final List<NoteView> allNotes = notesService.getAll(groupId);

        return ResponseEntity.ok(allNotes);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> toggleCompleted(@PathVariable final int id) {
        notesService.toggleCompleted(id);

        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/last-three")
    public ResponseEntity<?> getLastThreeNotes() {
        List<NoteView> lastThreeNotes = notesService.getLastThreeNotes();

        return ResponseEntity.ok(lastThreeNotes);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteNote(@PathVariable final int id) {
        notesService.deleteNote(id);

        return ResponseEntity.noContent().build();
    }
}
