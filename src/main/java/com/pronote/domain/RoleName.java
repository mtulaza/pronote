package com.pronote.domain;

public enum RoleName {
	ROLE_ADMIN, ROLE_USER
}
