package com.pronote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarterMain {
	public static void main(final String... args) {
		new SpringApplication(StarterMain.class).run(args);
	}

}
