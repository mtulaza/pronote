package com.pronote.groups.data;

public class CreateGroupData {
    private String groupName;

    public CreateGroupData() {
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
