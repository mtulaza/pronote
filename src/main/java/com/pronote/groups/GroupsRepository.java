package com.pronote.groups;

import com.pronote.domain.Group;
import com.pronote.support.jpa.CustomJpaRepository;

public interface GroupsRepository extends CustomJpaRepository<Group, Integer> {
}
