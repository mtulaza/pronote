package com.pronote.groups;

public class GroupNotFoundException extends RuntimeException {
    public GroupNotFoundException(final int id) {
        super("Cannot find group of id: " + id);
    }
}
