package com.pronote.groups;

import com.pronote.auth.AuthorizationService;
import com.pronote.domain.Group;
import com.pronote.groups.data.CreateGroupData;
import com.pronote.views.GroupView;
import com.pronote.views.GroupViewFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class GroupsService {
    @Autowired
    private GroupsRepository groupsRepository;
    @Autowired
    private GroupViewFactory groupViewFactory;
    @Autowired
    private AuthorizationService authorizationService;

    @Transactional
    public int createGroup(final CreateGroupData createGroupData) {
        Group group = new Group();
        group.setName(createGroupData.getGroupName());
        group.setNotes(Collections.emptyList());
        group.setOwner(authorizationService.getCurrentUser());

        final Group save = groupsRepository.save(group);

        return save.getId();
    }

    @Transactional(readOnly = true)
    public Optional<GroupView> findById(final int id) {
        Optional<Group> group = groupsRepository.findOne(id);

        return group.map(groupViewFactory::map);
    }

    @Transactional
    public void deleteGroup(final int id) {
        groupsRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public List<GroupView> getAll() {
        return groupsRepository.findAll().stream()
                .map(groupViewFactory::map)
                .collect(Collectors.toList());
    }
}
