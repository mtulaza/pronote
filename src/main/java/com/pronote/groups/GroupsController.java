package com.pronote.groups;

import com.pronote.groups.data.CreateGroupData;
import com.pronote.views.GroupView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = GroupsController.BASE_URI)
public class GroupsController {
    public final static String BASE_URI = "/group";

    @Autowired
    private GroupsService groupsService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getGroupById(@PathVariable final int id) {
        final Optional<GroupView> groupView = groupsService.findById(id);

        return groupView
                .map(ResponseEntity::ok)
                .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @PostMapping
    public ResponseEntity<?> createGroup(@RequestBody CreateGroupData createGroupData) {
        final int createdGroupId = groupsService.createGroup(createGroupData);

        return ResponseEntity
                .created(URI.create(BASE_URI + "/" + createdGroupId))
                .build();
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteGroup(@PathVariable final int id) {
        groupsService.deleteGroup(id);

        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<?> getAllGroups() {
        final List<GroupView> allGroups = groupsService.getAll();

        return ResponseEntity.ok(allGroups);
    }
}
