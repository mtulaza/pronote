package com.pronote.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfig {

    @Profile({ AppProfiles.DEV })
    @PropertySource("classpath:application-dev.properties") // Not loaded by naming convention
    @Configuration
    static class StandaloneDatabaseConfig {
        @Bean
        public DataSource dataSource(final Environment env) {
            final HikariDataSource ds = new HikariDataSource();
            ds.setJdbcUrl(env.getRequiredProperty("h2.jdbcurl"));
            ds.setUsername(env.getRequiredProperty("h2.username"));
            return ds;
        }
    }
}
