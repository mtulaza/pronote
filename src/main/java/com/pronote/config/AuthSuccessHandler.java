package com.pronote.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pronote.domain.RoleName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AuthSuccessHandler implements AuthenticationSuccessHandler {
    private final static Logger log = LoggerFactory.getLogger(AuthSuccessHandler.class);

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        log(authentication);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(response.getWriter(), new SuccessfulLoginMetadata(
                authentication.getName(),
                getRoles(authentication).contains(RoleName.ROLE_ADMIN.name())
        ));
    }

    private void log(Authentication authentication) {
        final String roles = getRoles(authentication).stream()
                .collect(Collectors.joining(", "));

        log.info("{} logged in successfuly, roles: {}", authentication.getName(), roles);
    }

    private Set<String> getRoles(Authentication authentication) {
        return authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet());
    }
}

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
class SuccessfulLoginMetadata {
    private String username;
    private boolean isAdmin;

    public SuccessfulLoginMetadata(String username, boolean isAdmin) {
        this.username = username;
        this.isAdmin = isAdmin;
    }
}