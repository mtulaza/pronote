package com.pronote.config;

public final class AppProfiles {
    public static final String DEV = "dev";

    private AppProfiles() {
    }
}
